package com.the.sample.app.grpc

import com.google.protobuf.Empty
import com.the.sample.app.grpc.UserServiceEndpointGrpc.UserServiceEndpointImplBase
import com.the.sample.app.grpc.UserServiceEndpointOuterClass.*
import com.the.sample.app.model.User
import com.the.sample.app.service.UserService
import io.grpc.stub.StreamObserver
import org.lognet.springboot.grpc.GRpcService
import java.util.*

@GRpcService
class UserServiceEndpointImpl(val userService: UserService) : UserServiceEndpointImplBase() {
    override fun findById(request: UserByIdRequest, responseObserver: StreamObserver<UserResponse>) {
        val userDto: UserDto? = Optional.ofNullable(request).
            map { request: UserByIdRequest -> request.id }.
            map { id: Long -> userService.findById(id) }.
            flatMap { res-> Optional.ofNullable(res).map { userData: User ->
            UserDto.newBuilder().setEmail(userData.email).setFullName(userData.fullName).build() } }.
            orElse(null)
        responseObserver.onNext(UserResponse.newBuilder().setUser(userDto).build())
        responseObserver.onCompleted()
    }

    override fun findByEmail(request: UserByEmailRequest, responseObserver: StreamObserver<UserResponse>) {
        val userDto: UserDto? = Optional.ofNullable(request).
            map { request: UserByEmailRequest -> request.email }.
            map { email: String -> userService.findByEmail(email) }.
            flatMap { res-> Optional.ofNullable(res).map { userData: User ->
                UserDto.newBuilder().setEmail(userData.email).setFullName(userData.fullName).build() } }.
            orElse(null)
        responseObserver.onNext(UserResponse.newBuilder().setUser(userDto).build())
        responseObserver.onCompleted()
    }

    override fun save(request: CreateUserRequest, responseObserver: StreamObserver<Empty>) {
        Optional.ofNullable(request).ifPresent { req: CreateUserRequest ->
            userService!!.save(
                User(email = req.email, fullName = req.fullName)
            )
        }
        responseObserver.onNext(Empty.newBuilder().build())
        responseObserver.onCompleted()
    }

    override fun update(request: UpdateUserRequest?, responseObserver: StreamObserver<Empty>) {
        Optional.ofNullable(request).ifPresent { req: UpdateUserRequest ->
            userService!!.save(
                User(id = req.id,
                    email = req.email,
                    fullName = req.fullName)
            )
        }
        responseObserver.onNext(Empty.newBuilder().build())
        responseObserver.onCompleted()
    }

    override fun deleteById(request: UserByIdRequest, responseObserver: StreamObserver<Empty>) {
        Optional.ofNullable(request).map { obj: UserByIdRequest -> obj.id }.ifPresent { id: Long? ->
            userService!!.deleteById(
                id!!
            )
        }
        responseObserver.onNext(Empty.newBuilder().build())
        responseObserver.onCompleted()
    }
}