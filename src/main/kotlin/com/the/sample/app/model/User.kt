package com.the.sample.app.model

import jakarta.persistence.*

@Entity
@Table(name = "UserData")
class User (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "identifier")
    var id: Long? = null,
    @Column(name="fullName", length = 40, nullable = false)
    var fullName: String,
    @Column(name="email", length = 50, unique = true, nullable = false)
    var email: String
)