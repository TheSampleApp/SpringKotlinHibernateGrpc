package com.the.sample.app

import com.the.sample.app.model.User
import com.the.sample.app.service.UserService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertNotNull

@ExtendWith(SpringExtension::class)
@ContextConfiguration(classes = [SpringHibernateCrudApplication::class])
class SpringHibernateCrudApplicationTest {
    @Autowired
    private val userService: UserService? = null

    @Test
    fun testCreateAndFind() {
        userService!!.save(User(fullName = "test user", email = "test@test.com"))
        assertNotNull(userService.findByEmail("test@test.com"))
    }
}